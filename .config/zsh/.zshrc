#title change "username" to your username
echo -n -e "\033]0;username@froggyOS\007"
stty stop undef
# sudo not required for some system commands
for command in gparted mount umount sv pacman updatedb su shutdown poweroff reboot powerpill ; do
	alias $command="sudo $command"
done; unset command

#history
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=~/.cache/zsh/history

#fancy prompt
PROMPT='%B%F{97}%n%B%F{182}@%B%F{175}%m%f%F{111}%B%~%b%f%B%F{183}➤ '
#sources
#source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh # if zsh autosugestions is installed then uncomment this line
source ~/.zsh/plugins/sudo.plugin.zsh
source ~/.zprofile

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)	

# arrows and home end fix
bindkey "\e[1;5D" backward-word
bindkey "\e[1;5C" forward-word
bindkey "\e[3;5~" kill-word
bindkey "\C-_"    backward-kill-word
bindkey "\e[3~" delete-char
bindkey "\e[H"  beginning-of-line
bindkey "\e[F"  end-of-line

# Verbosity
alias \
		cp="cp -iv" \
		mv="mv -iv" \
		rm="rm -vI" \
		bc="bc -ql" \
		mkd="mkdir -pv" \
		ffmpeg="ffmpeg -hide_banner"

# Colorize commands when possible.
alias \
		ls="ls -hN --color=auto --group-directories-first" \
		la="ls -hNa --color=auto --group-directories-first" \
		cla="clear ; neofetch ; ls -hNa --color=auto --group-directories-first" \
		grep="grep --color=auto" \
		diff="diff --color=auto" \
		ccat="highlight --out-format=ansi"

#music controls
alias \
		m="ncmpcpp" \
		pause="mpc pause" \
		play="mpc play" \
		next="mpc next" \
		prev="mpc prev" 


#shortened because long
alias \
		rb="reboot" \
		yt="yt-dlp --add-metadata -i" \
		yta="yt -x -f bestaudio/best" \
		ytv="youtube-viewer" 

#editing
alias \
		e="$EDITOR" \
		es="sudo $EDITOR" \
		ez="$EDITOR ~/.zshrc" \
		ezp="$EDITOR ~/.zprofile" \
		ex="$EDITOR ~/.xinitrc" \
		exp="$EDITOR ~/.xprofile" \
		exr="$EDITOR ~/.xresources" \
		ek="$EDITOR ~/.config/kitty/kitty.conf" \
		en="$EDITOR ~/.config/neofetch/config.conf" \
		eb="$EDITOR ~/.config/micro/bindings.json" 

#terminal hacks
alias \
		c="clear ; neofetch" \
		d="du -hs" \
		z="exec zsh" \
		f="ranger" \
		fh="ranger ~/" \
		gt="gotop" \
		tt="btop" \
		nt="nvtop" \
		ht="htop" 

#package management
alias \
		up="pacman -Sy && powerpill -Su && paru -Su" \
		upd="pacman -Syu " \
		p="pacman" \
		pr="pacman -R" \
		prc="pacman -Rnsc" \
		pq="pacman -Qs" \
		pi="pacman -S" \
		pii="paru -S" \
		pii="paru -S" \
		pc="sudo paccache -r -k1 -u -vvv" 

#startup
neofetch
